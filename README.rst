==============
Docker manager
==============


.. image:: https://img.shields.io/pypi/v/dk.svg
        :target: https://pypi.python.org/pypi/dk

.. image:: https://img.shields.io/travis/mattberjon/dk.svg
        :target: https://travis-ci.com/mattberjon/dk

.. image:: https://readthedocs.org/projects/dk/badge/?version=latest
        :target: https://dk.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Manage remote docker instance


* Free software: Apache Software License 2.0
* Documentation: https://dk.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
