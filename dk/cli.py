"""Console script for dk."""
from .dk import Container
from .utils import print_pretty_json
from rich.console import Console
import sys
import click


class CustomMultiGroup(click.Group):
    """Defines aliases for group commands"""

    def group(self, *args, **kwargs):
        """Behaves the same as `click.Group.group()` except if passed
        a list of names, all after the first will be aliases for the first.
        """
        def decorator(f):
            aliased_group = []
            if isinstance(args[0], list):
                # we have a list so create group aliases
                _args = [args[0][0]] + list(args[1:])
                for alias in args[0][1:]:
                    grp = super(CustomMultiGroup, self).group(
                        alias, *args[1:], **kwargs)(f)
                    grp.short_help = "Alias for '{}'".format(_args[0])
                    aliased_group.append(grp)
            else:
                _args = args

            # create the main group
            grp = super(CustomMultiGroup, self).group(*_args, **kwargs)(f)

            # for all of the aliased groups, share the main group commands
            for aliased in aliased_group:
                aliased.commands = grp.commands

            return grp

        return decorator


# Main command: dk
@click.group(
        context_settings=dict(help_option_names=['-h', '--help']),
        cls=CustomMultiGroup)
def main():
    """ Docker manager CLI """

# Group command: version
@main.command('version', help='version of dk program and Docker info')
def version():
    container = Container()
    version_data = container.docker_version()
    print_pretty_json(version_data)

# Group command: context
@main.command('context', help='Context management')
def context():
    """Manage contextes"""
    click.echo('Context management')

# Group commands: container/c
#  @main.group(['container', 'c'], help='Container management')
@main.group('container', help='Container management')
def container():
    """Manage container"""


@container.command('ls', help='List all containers')
@click.option('-r', '--raw', is_flag=True, help='Display raw json info')
def list(raw):
    container = Container()
    container.list(is_raw=raw)


@container.command('ip', help='Get container\'s IP address')
@click.argument('container_name')
def ip_addr(container_name):
    container = Container()
    ip_addr = container.get_ip_addr(container_name)
    print(ip_addr)


@container.command('info', help='Get container\'s info')
@click.argument('container_name')
@click.option('-r', '--raw', is_flag=True, help='Display raw json information')
def inspect(container_name, raw):
    container = Container()
    container_info = container.get_info(container_name, raw=raw)

    console = Console()
    if raw:
        print_pretty_json(container_info)
    else:
        console.print(container_info)


@container.command('prune', help='Delete all exited containers')
def prune():
    container = Container()
    container.prune()


# Exec group commands
@main.command('exec', help='Execute a command on a given container')
def exec():
    """Execute a command on a specific container"""
    click.echo('run a command')


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
