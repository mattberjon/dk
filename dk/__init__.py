"""Top-level package for Docker manager."""

__author__ = """Matthieu Berjon"""
__email__ = 'matthieu@berjon.net'
__version__ = '0.1.0'
