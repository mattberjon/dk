# -*- coding: utf-8 -*-
"""dk module.

This module manages the actions related to a remote Docker engine
It is used to perform actions on the remote engine or aggregate information
from it to display them to the user.

Todo:
    * add logging and debug capabilities
    * add configuration file
    * add utokentome management
    * add command to swith context (site [bgl, mts, sph], env
    [prod, integ, rec), exposure [priv, pub]
    * add completion
    * add test commands (for exemple, connect to a toolbox and run
    a nc command)
    * add try and catch methods to manage exceptions
    * find a better and homogeneous way to handle getting data through dicts
    * try to return always to the same kind of object to display to avoid
    returning different object in a same method
"""


import configparser
import docker
from docker import errors
import json
from rich import box
from rich.console import Console
from rich.table import Table


class Connection():
    """Connect to a remote or local instance

    Attributes:
        No attributes
    """

    def __init__(self):
        """Object initialisation

        Args:
            No params

        Note:
            This method initialize the object by fetching the `docker_base_url`
            from a configuration file in INI format.

            [defaults]
            docker_base_url = your_connection_method

        """
        config = configparser.ConfigParser()
        config.read('config.ini')
        self.docker_base_url = config['defaults']['docker_base_url']

    def docker_connection(self):
        """Connection through the Docker client

        Args:
            No params

        Returns:
            DockerClient object
        """
        return docker.DockerClient(base_url=self.docker_base_url)

    def api_connection(self):
        """Connection through the API client

        Args:
            No params

        Returns:
            APIClient object
        """
        return docker.APIClient(base_url=self.docker_base_url)


class Container():
    """Container class

    Manages different operations on containers.

    Todo:
        * add stats/top command
        * add logs command
        * add rm command (with regex)
    """

    def get_ip_addr(self, container_name):
        """Get the container IP address

        Args:
            container_name (str): container name or ID

        Returns:
            A string containing the IP address
        """
        connection = Connection()
        client = connection.docker_connection()
        try:
            container = client.containers.get(container_name)
            ip_address = container.attrs['NetworkSettings']['IPAddress']
            return ip_address
        except errors.NotFound:
            return 'Error 404. No such container: {}'.format(container_name)

    def get_info(self, container_name, raw=False):
        """Get information on a given container

        Args:
            container_name (str):   container name or ID
            raw (bool):             flag to choose format

        Returns:
            A dockerClient object or a Console object
        """
        connection = Connection()
        client = connection.api_connection()
        container_info = client.inspect_container(container_name)

        if raw:
            return container_info
        else:
            ports = list()
            for port in container_info['NetworkSettings']['Ports']:
                ports.append(port)

            if 'Health' in container_info['State'].keys():
                if container_info['State']['Health']['Status'] == 'starting':
                    health = "starting :thinking_face:"
                elif container_info['State']['Health']['Status'] == 'healthy':
                    health = "healthy :bottle_with_popping_cork:"
                else:
                    health = "unhealthy :cry:"
            else:
                health = '-'

            container_info_t = Table(
                    show_header=True,
                    box=box.SIMPLE,
                    header_style='bold magenta'
            )
            container_info_t.add_column('Parameter', style='dim')
            container_info_t.add_column('Value')
            container_info_t.add_row('Name', container_info['Name'][1:])
            container_info_t.add_row('ID', container_info['Id'][:12])
            container_info_t.add_row(
                    'IP address',
                    container_info['NetworkSettings']['IPAddress']
            )
            container_info_t.add_row('Port', ''.join(ports))
            container_info_t.add_row(
                    'Image',
                    container_info['Config']['Image']
            )
            container_info_t.add_row('Site', '-')
            container_info_t.add_row('Environment', '-')
            container_info_t.add_row('Exposure', '-')
            container_info_t.add_row('CPU quota', '-')
            container_info_t.add_row('Memory quota', '-')
            container_info_t.add_row('Volumes', '-')
            container_info_t.add_row('VXlan', '-')
            container_info_t.add_row(
                    'Status',
                    container_info['State']['Status']
            )
            container_info_t.add_row('Health', health)
            container_info_t.add_row('Uptime', '-')

            return container_info_t

    def docker_version(self):
        """Return Docker engine info

        Args:
            None

        Returns:
            DockerClient object
        """
        connection = Connection()
        client = connection.api_connection()
        version_data = client.version()
        return version_data

    def list(self, is_raw=False):
        """List all available containers

        Args:
            is_raw (bool):  Flag to return raw data instead of formatted.

        Returns:
            DockerClient object or RichConsole object

        Todo:
            * restrict to a default filter such as SDE or even only Gatape
        """
        connection = Connection()
        client = connection.docker_connection()
        containers = client.containers.list(all=True)

        if not is_raw:
            # Output definition
            console = Console()
            table = Table(
                    show_header=True,
                    box=box.SIMPLE,
                    header_style='bold magenta'
            )
            table.add_column('Stack', style='dim')
            table.add_column('ID', style='dim')
            table.add_column('Name', style='dim')
            table.add_column('IP address', style='dim')
            table.add_column('Ports', style='dim')
            table.add_column('Status', style='dim')

            for container in containers:

                if container.attrs['State']['Status'] == 'exited':
                    status = "[bold][red]{}[/red][/bold]".format(
                            container.attrs['State']['Status'])
                else:
                    status = container.attrs['State']['Status']

                if container.attrs['NetworkSettings']['IPAddress']:
                    ip_addr = container.attrs['NetworkSettings']['IPAddress']
                else:
                    ip_addr = '-'

                ports = list()
                for port in container.attrs['NetworkSettings']['Ports']:
                    ports.append(port)

                if 'org.tagada.stack' in container.attrs['Config']['Labels']:
                    stack = \
                        container.attrs['Config']['Labels']['org.tagada.stack']
                else:
                    stack = '-'

                table.add_row(
                        stack,
                        container.attrs['Id'][:12],
                        container.attrs['Name'][1:],
                        ip_addr,
                        ''.join(ports),
                        status
                )

            if table.rows:
                console.print(table)
            else:
                console.print('[bold]No containers are running[/bold]')
        else:
            for container in containers:
                print(json.dumps(container.attrs, indent=4, sort_keys=True))

    def prune(self):
        """Remove all excited containers
        """
        connection = Connection()
        client = connection.docker_connection()
        res = client.containers.prune()

        if res['ContainersDeleted'] is not None:
            print(json.dumps(res, indent=4, sort_keys=True))
        else:
            print('No containers to delete')


class Exec():

    def run_cmd(self, cmd):
        return NotImplemented

    def connect_to_container(self, container_name):
        return NotImplemented

    def check_flow(self, flow):
        return NotImplemented
