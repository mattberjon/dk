import json


def print_pretty_json(json_data):
    print(json.dumps(json_data, indent=4, sort_keys=True))
